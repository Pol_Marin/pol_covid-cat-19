import React, { useState, useEffect } from "react";
import { Container } from 'reactstrap';
import {
  BrowserRouter,
  NavLink,
  Switch,
  Route,
} from "react-router-dom";

import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";
import "../node_modules/react-vis/dist/style.css";


import Home from './Home';
import Barcelones from './Barcelones';
import Comarcas from './Comarcas';
import Comparativa from './Comparativa';
import NotFound from './NotFound';


function App() {

  /* STATES */

  //Control de carga y error en el fetch
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(true);

  //Comarca a procesar
  const [content, setContent] = useState([]);

  //Numero del codigo de la comarca
  const [numComarca, setNumComarca] = useState(0);

  //comarcas a comparar
  const [com1, setCom1] = useState([]);
  const [com2, setCom2] = useState([]);


  //Petición de datos
  useEffect(() => {
    const apiUrl = `https://analisi.transparenciacatalunya.cat/resource/jvut-jxu8.json?codi=${numComarca}&residencia=No`;
    fetch(apiUrl)
      .then((response) => response.json())
      .then((data) => {
        setContent(data);
        setLoading(false);
      })
      .catch((error) => setError(true));

  }, [numComarca]);

  //Asignar comunidades a comparar
  useEffect(() => {

    if (com1.length === 0) setCom1(content);
    else if (com2.length === 0) setCom2(content);
    else return;

  }, [content])

  
  //Manejadores de error y carga
  if (error) {
    return <h3>Se ha producido un error...</h3>;
  }
  if (loading) {
    return <i className="fa fa-spinner fa-spin fa-2x fa-fw" />;
  }


  //Funcion para ordenar fechas y filtrar
  const comarca = content
    .sort((a, b) => (a.data_ini > b.data_ini) ? 1 : -1)
    .filter((el, idx) => idx % 7 === 0)

  return (
    <BrowserRouter>
      <Container>
        <br />
        <ul className="nav nav-tabs d-flex justify-content-center">
          <li className="nav-item">
            <NavLink exact className="nav-link" to="/">
              HOME
            </NavLink>
          </li>
          <li>
            <NavLink exact className="nav-link" to="/barcelones">
              BARCELONÈS
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/comarcas">
              TRIA COMARCA
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/comparativa">
              COMPARATIVA
            </NavLink>
          </li>
        </ul>
        <br />
        <br />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/barcelones" render={() => <Barcelones comarca={comarca} setNum={setNumComarca} />} />
          <Route path="/comarcas" render={() => <Comarcas comarca={comarca} setNum={setNumComarca} />} />
          <Route path="/comparativa" render={() => <Comparativa c1={com1} c2={com2} setC1={setCom1} setC2={setCom2} setNum={setNumComarca} />} />
          <Route component={NotFound} />
        </Switch>
      </Container>
    </BrowserRouter>
  );
}

export default App;
