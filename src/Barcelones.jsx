import React from "react";
import { Container, Row, Col, Table } from 'reactstrap';

import Grafico from "./Grafico"


const Barcelones = (props) => {

    //Envia numero Barcelones para recibir sus datos
    props.setNum('13')

    //Genera tabla con datos del Barcelones
    const barcelonesContent = props.comarca.map((el) => {

        return (

            <tbody>
                <tr>
                    <td>{el.data_ini.split("T")[0].split("-").reverse().join("/")}</td>
                    <td>{el.data_fi.split("T")[0].split("-").reverse().join("/")}</td>
                    <td>{el.ingressos_total}</td>
                    <td>{el.casos_confirmat}</td>
                </tr>
            </tbody>
        )
    })

    return (
        <>
            <Container>
                <Row>
                    <h1>DATOS BARCELONÉS</h1>
                </Row>
                <Row>
                    <Col>
                        <h3>TABLA DE DATOS SEMANALES</h3>
                        <Table bordered hover className="mt-5 tabla">
                            <thead>
                                <tr>
                                    <th>Data i</th>
                                    <th>Data f</th>
                                    <th>Ingressos</th>
                                    <th>Confirmats</th>
                                </tr>
                            </thead>
                            {barcelonesContent}
                        </Table>
                    </Col>
                    <Col>
                        <h3>GRÁFICO BARCELONÉS</h3>
                        <div className="mt-5">
                                <Grafico datos={props.comarca} datos2={[]}/>
                        </div>
                    </Col>
                </Row>
            </Container>
        </>
    );
};

export default Barcelones;
