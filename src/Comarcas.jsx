import React, { useEffect, useState } from "react";
import { 
    Dropdown, 
    DropdownToggle, 
    DropdownMenu, 
    DropdownItem, 
    Container, 
    Row, 
    Col } from 'reactstrap';

import { comarques } from "./comarques.js"
import Grafico from "./Grafico.jsx";


const Comarcas = (props) => {

    //Controladores del Dropdown
    const [dropdownOpen, setDropdownOpen] = useState(false);
    const toggle = () => setDropdownOpen(prevState => !prevState);

    //Control del mensaje (comarca seleccionada)
    const [mensaje, setMensaje] = useState(' - ');

    
    //Reset gráfico
    useEffect(() => {
        props.setNum(0);
    },[])


    //Creacion de seleccionables dropdown + envia num comarca clickada + cambia mensaje
    const listaComarcas = comarques.map(el => {

        const showGrafico = () => {
            props.setNum(el.codi);
            setMensaje(el.nom);
        }
        return (
            <DropdownItem onClick={showGrafico}>{el.nom}</DropdownItem>
        )
    })

    return (
        <>
            <Container>
                <Row>
                    <h1>DATOS COMARCAS CATALUÑA</h1>
                </Row>
                <Row>
                    <Col>
                        <h4 className="mt-4">Seleccione una comarca:</h4>
                        <Dropdown className="mt-3" direction="right" isOpen={dropdownOpen} toggle={toggle}>
                            <DropdownToggle color="primary" caret>
                                Comarcas
                            </DropdownToggle>
                            <DropdownMenu>
                                {listaComarcas}
                            </DropdownMenu>
                        </Dropdown>
                        <div className="com-selected">
                            <h5>COMARCA SELECCIONADA:  </h5>
                            <h3><strong>{mensaje}</strong></h3>
                        </div>
                    </Col>
                    <Col>
                        <div className="mt-5">
                            <Grafico datos={props.comarca} datos2={[]}/>
                        </div>
                    </Col>
                </Row>
            </Container>

        </>
    );
}

export default Comarcas;
