import React, { useEffect } from "react";
import { Col, Row, Container } from 'reactstrap';

import Select from "./Select";
import Grafico from "./Grafico";

const Comparativa = (props) => {

    //Reset gráfico
    useEffect(() => {
        props.setC1([]);
        props.setC2([]);
    }, [])


    return (
        <>
            <Container>
                <Row>
                    <h1>COMPARA COMARCAS</h1>
                </Row>
                <Row>
                    <Col>
                        <Select campo="PRIMERA" setNum={props.setNum} />
                        <Select campo="SEGUNDA" setNum={props.setNum} />
                    </Col>
                    <Col>
                        <div className="mt-5">
                            <Grafico datos={props.c1} datos2={props.c2} />
                        </div>
                    </Col>
                </Row>
            </Container>
        </>
    );
}

export default Comparativa;