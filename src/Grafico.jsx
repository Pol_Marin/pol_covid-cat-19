import React from "react";

import {
    XYPlot,
    XAxis,
    YAxis,
    HorizontalGridLines,
    LineSeries,
    VerticalGridLines,
} from "react-vis";


const Grafico = (props) => {

    //Genera el contenido del gráfico
    const graficComarca =(com) => com.map(el => {

        return (
            { x: new Date(el.data_ini), y: el.ingressos_total }
        )
    })

    return (
        <>
            <XYPlot xType="time" yDomain={[0, 500]} width={500} height={300}>
                <HorizontalGridLines />
                <VerticalGridLines />
                <XAxis title="Fechas" />
                <YAxis title="Casos confirmados" />
                <LineSeries data={graficComarca(props.datos)} color="blue"/>
                <LineSeries data={graficComarca(props.datos2)} color="red"/>
            </XYPlot>
        </>
    );
}

export default Grafico;