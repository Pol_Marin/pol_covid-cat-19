import React from "react";
import { Container } from 'reactstrap';

import { Link } from "react-router-dom";


const Home = () => {

    return (
        <>
            <Container>
                <div className="home mt-4">
                    <h1>POL COVID-CAT-19</h1>
                    <div className="mt-5">
                        <p>Bienvenidos a POL COVID-CAT-19, un espacio donde podrás consultas información sobre: </p>
                        <ul>
                            <li>
                                <Link to="/barcelones">Los datos de cada semana de la comarca del Barcelonés</Link>
                            </li>
                            <li>
                                <Link to="/comarcas">Los gráficos de la casuística de cada comarca de Cataluña</Link>
                            </li>
                            <li>
                                <Link to="/comparativa">Una grafica comparativa entre comarcas</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </Container>
        </>
    );
};

export default Home;
