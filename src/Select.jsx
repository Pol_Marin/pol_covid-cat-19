import React from "react";
import { FormGroup, Label, Input } from 'reactstrap';


import { comarques } from "./comarques.js";

const Select = (props) => {

    //Generar lista seleccionables (comarcas) 
    const listaComarcas = comarques.map(el => {

        let getComarca = () => {
            props.setNum(el.codi);
        }

        return (
            <option onClick={getComarca}>{el.nom}</option>
        )
    })

    return (
        <>
            <FormGroup className="mt-4" >
                <Label for="exampleSelectMulti">SELECCIONE {props.campo} COMARCA:</Label>
                <Input type="select" name="selectMulti" id="exampleSelectMulti" multiple>
                    {listaComarcas}
                </Input>
            </FormGroup>
        </>
    );
}

export default Select;